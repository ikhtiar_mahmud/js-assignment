function goBack() {
    window.history.back();
}

function goForward() {
    window.history.forward();
}

//check cookie status
let cookieStatus = window.navigator.cookieEnabled;
let appName = window.navigator.appName;
let appCode = window.navigator.appCodeName;
let appProduct = window.navigator.appProduct;
let appVersion = window.navigator.appVersion;
let userAgent = window.navigator.userAgent;
let platform = window.navigator.platform;
let language = window.navigator.language;
let onLine = window.navigator.onLine;
let javaEnabled = window.navigator.javaEnabled;

document.getElementById('cStatus').innerHTML = "Cookie Status : " + cookieStatus;
document.getElementById('appName').innerHTML = " Browser Name : " + appName;
document.getElementById('appCode').innerHTML = " Browser Code : " + appCode;
document.getElementById('appProduct').innerHTML = "Browswer Product : " + appProduct;
document.getElementById('appVersion').innerHTML = "Browser Version : " + appVersion;
document.getElementById('userAgent').innerHTML = "User Info : " + userAgent;
document.getElementById('platform').innerHTML = "Platform : " + platform;
document.getElementById('language').innerHTML = "Language : " + language;
document.getElementById('online').innerHTML = "Online Status : " + onLine;
document.getElementById('javaEnabled').innerHTML = "Java Enabled : " + javaEnabled;