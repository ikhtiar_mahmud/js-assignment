// ==============Two properties can be used to determine the size of the browser window(in pixels)=======
let wInnerWidth  = window.innerWidth; 
let wInnerHeight = window.innerHeight;

window.document.getElementById('wInnerWidth').innerText  = "Window width is : " + wInnerWidth;
window.document.getElementById('wInnerHeight').innerText = "Window Height is : " + wInnerHeight;


//=================window open(),close(),moveTo(),resizeTo()==============

//syntax : window.open(url,name,spec,replace)
//url  : what we want to open.
/*name : 
    1. _blank  : url open a new window or new toolbar. This is default. 
    2. _self   : url open in current tab.
    3. _parent : url open in parent frame.
    4. _top    : url replace any farmset may be loaded.
*/
//spec : A comma-separated list of items, no whitespaces.
/*replace : Specifies whether the URL creates a new entry or replaces the current entry in the history list.
    1. true : URL replaces the current document in the history list
    2.false : URL creates a new entry in the history list.
*/

let newWindow;
function wOpen(){
    //  window.open("http://mitul.cf");
    newWindow = window.open("http://mitul.cf","newWindow","width:200,Height:200",true);

}

function wClose(){
    newWindow.close();
}

function wMoveTo(){
    newWindow.moveTo(300,100);
    newWindow.focus(); 
}

function wResizeTo(){
    newWindow.resizeTo(200,100);
}
/* Question :
    1. what for use _parent and _top.
    2.moveTo and resizeTo is not work.
*/

//=================window alert(),confirm(),propmt(),print()==============

function wAlert(){
    window.alert("Pondit is best institute for training");
}

function wConfirm(){
    window.confirm("Are You Sure!");
}

function wPrompt(){
    window.prompt("Please Provide Your Phone No For Contact With You!");
}

function wPrint(){
    window.print();
}