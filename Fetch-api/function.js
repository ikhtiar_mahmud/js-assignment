function loadData(){
	fetch("https://jsonplaceholder.typicode.com/users")
		.then((resp)=>resp.text())
		.then(function(data){
			let usersData= eval(data)
		    User.htmlTable.display(usersData, '#root');
        })
}

document.getElementById('clickBtn').addEventListener('click', loadData)


