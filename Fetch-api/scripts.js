// let colors =  [
//     {
//       "color": "black",
//       "category": "hue",
//       "type": "primary",
//       "code": "#yyfgfhg0",
//     },
//     {
//       "color": "white",
//       "category": "value",
//       "type": "primary",
//       "code": "#yyfgfhg0",
//     },
//     {
//       "color": "red",
//       "category": "hue",
//       "type": "primary",
//       "code": "#yyfgfhg0",
//     },
//     {
//       "color": "blue",
//       "category": "hue",
//       "type": "primary",
//       "code": "#yyfgfhg0",
//     },
//     {
//       "color": "yellow",
//       "category": "hue",
//       "type": "primary",
//       "code": "#yyfgfhg0",
//     },
//     {
//       "color": "green",
//       "category": "hue",
//       "type": "secondary",
//       "code": "#yyfgfhg0",
//     },
//   ]


const User = {};
User.htmlTable = {
	buildTh: function(column){
		let thText = document.createTextNode(column);
		let th = document.createElement("th")
        th.appendChild(thText);
        
		return th;
    },
    
	buildTableHead:function(tHeads){
		let tr = document.createElement("tr");
		for(let tHead of tHeads){
			let th = this.buildTh(tHead);
			tr.appendChild(th);
        }
        
		return tr;
	},

	buildTbody:function(column){
		let tdCell = document.createTextNode(column);
		let td = document.createElement("td");
        td.appendChild(tdCell);
        
		return td;
	},
    buildTd:function(column){
    	let text = document.createTextNode(column);
    	let td = document.createElement("td");
    	td.appendChild(text);
    	return td;
    },
    buildTr:function(objRowData){
        console.log(objRowData);
    	let tr = document.createElement("tr");
    	for(let data in objRowData){
    		td = this.buildTd(objRowData[data]);
    		tr.appendChild(td);
        }
        
    	return tr;
    },
    buildTableBody: function(collection){
    	let tbody = document.createElement('tbody');
    	for(let data of collection){
    		let tr = this.buildTr(data);
    		tbody.appendChild(tr);
        }
        
    	return tbody;
    },
    buildtable:function(collection){
    	let table = document.createElement("table");
    	let titles = Object.keys(collection[0]);
    	let thead = this.buildTableHead(titles);
    	let tBody = this.buildTableBody(collection);
    	table.appendChild(thead);
    	table.appendChild(tBody);
        table.setAttribute('border','1');
        
    	return table;
    },
    display: function(users, domLocation){
    	let container = document.querySelector(domLocation);
        let table = this.buildtable(users);
        container.appendChild(table);
        
    	return true;
    }
};

// Cat.htmlTable.display(colors, '#root');