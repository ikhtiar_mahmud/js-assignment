let getInfo =
{
  "squadName": "Super hero squad",
  "homeTown": "Metro City",
  "formed": 2016,
  "secretBase": "Super tower",
  "active": true,
  "members": [
    {
      "name": "Molecule Man",
      "age": 29,
      "secretIdentity": "Dan Jukes",
      "powers": [
        "Radiation resistance",
        "Turning tiny",
        "Radiation blast"
      ]
    },
    {
      "name": "Madame Uppercut",
      "age": 39,
      "secretIdentity": "Jane Wilson",
      "powers": [
        "Million tonne punch",
        "Damage resistance",
        "Superhuman reflexes"
      ]
    },
    {
      "name": "Eternal Flame",
      "age": 1000000,
      "secretIdentity": "Unknown",
      "powers": [
        "Immortality",
        "Heat Immunity",
        "Inferno",
        "Teleportation",
        "Interdimensional travel"
      ]
    }
  ]
}


const Info = {};

Info.htmlTable = {
  buildTh: function(column) {
    let thText = document.createTextNode(column);
    let th = document.createElement("th");
    th.appendChild(thText);

    return th;
  },

  buildTableHead: function(tHeads) {
    let tr = document.createElement("tr");
    for (let tHead of tHeads) {
      let th = this.buildTh(tHead);
      tr.appendChild(th);
    }

    return tr;
  },


  buildTd: function(column) {
    let text = document.createTextNode(column);
    let td = document.createElement("td");
    td.appendChild(text);

    return td;
  },

  buildli: function(list) {
    let text = document.createTextNode(list);
    let li = document.createElement("li");
    li.appendChild(text);

    return li;
  },

  buildul: function(lastIndex) {
    let ul = document.createElement("ul");
    for (let item of lastIndex) {
      let li = this.buildli(item);
      ul.appendChild(li);
    }

    return ul;
  },

  buildTr: function(objData) {
    let lastChild = this.buildul(objData.powers);
    let tr = document.createElement("tr");
    for (let item in objData) {
      td = this.buildTd(objData[item]);
      tr.appendChild(td);
      //td.children[4].filter(":first").text("");
      td.insertBefore(lastChild, td.children[4]);
    
    }

    return tr;
  },

  buildTableBody: function(collection) {
    let tbody = document.createElement("tbody");
    for (let data of collection) {
      let tr = this.buildTr(data);
      tbody.appendChild(tr);
    }

    return tbody;
  },

  buildtable: function(collection) {
    let table = document.createElement("table");
    let titles = Object.keys(collection[0]);
    let thead = this.buildTableHead(titles);
    let tBody = this.buildTableBody(collection);
    table.appendChild(thead);
    table.appendChild(tBody);
    table.setAttribute("border", "1");

    return table;
  },

  display: function(info, location) {
    let container = document.querySelector(location);
    let table = this.buildtable(info.members);
    container.appendChild(table);

    return true;
  }
};

Info.htmlTable.display(getInfo, '#root');
