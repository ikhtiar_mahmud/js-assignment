let actors = [
    {
        "name": "Tom Cruise",
        "age": 56,
        "Born_at": "Syracuse, NY",
        "Birthdate": "July 3, 1962",
      },
      {
        "name": "Robert Downey Jr.",
        "age": 53,
        "Born_at": "New York City, NY",
        "Birthdate": "April 4, 1965",
      },  
      {
        "name": "Jacky Sen Jr.",
        "age": 535,
        "Born_at": "New York City, NY",
        "Birthdate": "April 4, 1965",
      }  
]

  
let actorTableRow = "";
for(let actor of actors){
    actorTableRow += `<tr>
            <td>${actor.name}</td>
            <td>${actor.age}</td>
            <td>${actor.Born_at}</td>
            <td>${actor.Birthdate}</td>
          </tr>`
}
document.getElementById('actor').innerHTML += actorTableRow;