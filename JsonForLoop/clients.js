let clients = [
    {
      "name": "Dunlap Hubbard",
      "age": 36,
      "gender": "male",
      "company": "CEDWARD",
    },
    {
      "name": "Kirsten Sellers",
      "age": 24,
      "gender": "female",
      "company": "EMERGENT",
    },
    {
      "name": "Mohi Uddin",
      "age": 30,
      "gender": "male",
      "company": "ORGANICA",
    },
    {
        "name": "Enamul Hoque",
        "age": 30,
        "gender": "male",
        "company": "ORGANICA",
      },
      {
        "name": "Rashed Alam",
        "age": 30,
        "gender": "male",
        "company": "ORGANICA",
      },
      {
        "name": "Sajjad Hossain",
        "age": 30,
        "gender": "male",
        "company": "ORGANICA",
      }
  ]

let clientTableRow = "";
for(let client of clients){
    clientTableRow += `<tr>
            <td>${client.name}</td>
            <td>${client.age}</td>
            <td>${client.gender}</td>
            <td>${client.company}</td>
          </tr>`
}
document.getElementById('client').innerHTML += clientTableRow;