// let developers = ['Rashed','Mitul','Mohi','Sajjad'];


// for(let developer in developers){
//     console.log(developer.value);
// }

let countries = [
    {"name": "Afghanistan", "code": "AF"},
    {"name": "Åland Islands", "code": "AX"},
    {"name": "Albania", "code": "AL"},
    {"name": "Algeria", "code": "DZ"},
    {"name": "American Samoa", "code": "AS"},
    {"name": "AndorrA", "code": "AD"},
    {"name": "Angola", "code": "AO"},
    {"name": "Anguilla", "code": "AI"},
    {"name": "Antarctica", "code": "AQ"},
    {"name": "Antigua and Barbuda", "code": "AG"},
    {"name": "Argentina", "code": "AR"},
    {"name": "Armenia", "code": "AM"},
    {"name": "Aruba", "code": "AW"},
    {"name": "Australia", "code": "AU"},
    {"name": "Austria", "code": "AT"},
]

let countryTableRow = "";
for(let country of countries){
    countryTableRow += `<tr>
            <td>${country.name}</td>
            <td>${country.code}</td>
          </tr>`
}
document.getElementById('country').innerHTML += countryTableRow;