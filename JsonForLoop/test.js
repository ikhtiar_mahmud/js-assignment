let sampleJson = [
    {
      "id": "5973782bdb",
      "isActive": true,
      "balance": "$1,446.35",
      "age": 32,
      "eyeColor": "green",
      "name": "Logan Keller",
      "gender": "male",
      "company": "ARTIQ",
      "email": "logankeller@artiq.com",
      "phone": "+1 (952) 533-2258",
      "friends": [
        {
          "id": 0,
          "name": "Colon Salazar"
        },
        {
          "id": 1,
          "name": "French Mcneil"
        },
        {
          "id": 2,
          "name": "Carol Martin"
        }
      ],
    }
  ]

let sampleTableRow = "";
for(let sample of sampleJson){
    sampleTableRow += `<tr>
            <td >${sample.id}</td>
            <td >${sample.isActive}</td>
            <td >${sample.balance}</td>
            <td >${sample.age}</td>
            <td >${sample.eyeColor}</td>
            <td >${sample.name}</td>
            <td >${sample.gender}</td>
            <td >${sample.company}</td>
            <td >${sample.email}</td>
            <td >${sample.phone}</td>`

            for (let x of sample.friends){
                // console.log(`<td>${x.id}</td>
                // <td>${x.name}</td>`) 
                `<td>${x.id}</td>
                <td>${x.name}</td>`            
            }
          `</tr>`
}
document.getElementById('sampleData').innerHTML += sampleTableRow;