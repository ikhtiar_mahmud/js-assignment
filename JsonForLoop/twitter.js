let twitters = [
    {
    "created_at": "Thu Jun 22 21:00:00 +0000 2017",
    "text": "Creating a Grocery List Manager Using Angular, Part 1: Add &amp; Display Items https://t.co/xFox78juL1 #Angular",
    "truncated": false,
    "entities": {
      "hashtags": {
        "text": "Angular",
        "indices": [103, 111]
      },
      "urls": {
        "url": "https://t.co/xFox78juL1",
        "expanded_url": "http://buff.ly/2sr60pf",
        "display_url": "buff.ly/2sr60pf",
        "indices": [79, 102]
      }
    },
    "source": "<a href=\"http://bufferapp.com\" rel=\"nofollow\">Buffer</a>",
    "user": {
      "description": "Keep up with JavaScript tutorials, tips, tricks and articles at SitePoint.",
      "url": "http://t.co/cCH13gqeUK",
      "entities": {
          "urls": {
            "url": "http://t.co/cCH13gqeUK",
            "expanded_url": "http://sitepoint.com/javascript",
          }
      },
      "protected": false,
      "followers_count": 2145,
    },
  }]

  
let twitterTableRow = "";
for(let twitter of twitters){
    twitterTableRow += `<tr>
            <td>${twitter.created_at}</td>
            <td>${twitter.text}</td>
            <td>${twitter.truncated}</td>
            <td>${twitter.entities.hashtags.text}</td>
            <td>${twitter.entities.hashtags.indices}</td>
            <td>${twitter.entities.urls.url}</td>
            <td>${twitter.entities.urls.expanded_url}</td>
            <td>${twitter.entities.urls.display_url}</td>
            <td>${twitter.entities.urls.indices}</td>
            <td>${twitter.source}</td>
            <td>${twitter.user.description}</td>
            <td>${twitter.user.url}</td>
            <td>${twitter.user.entities.urls.expanded_url}</td>
            <td>${twitter.user.protected}</td>
            <td>${twitter.user.followers_count}</td> 
          </tr>`
}
document.getElementById('twitter').innerHTML += twitterTableRow;