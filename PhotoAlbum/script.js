
const photo = {};

photo.htmlCard = 
{
    buildImg : function (imageUrl) {
        let img = document.createElement('img');
        img.setAttribute("src",imageUrl);
        img.setAttribute("style",'width:100%');

        return img;
    },

    buildTitle : function (text) {
        let div = document.createElement('div');
        let textNode = document.createTextNode(text);
        let p = document.createElement('p');
        let title = p.appendChild(textNode);
        div.setAttribute("class","container");
        div.appendChild(title);

        return div;
    },

    buildCard : function (collection) {
        let card = document.createElement('div');
        card.setAttribute('class','card');
        for(let item of collection) {
            let img = this.buildImg(item.url);
            let p = this.buildTitle(item.title);

            card.appendChild(img);
            card.appendChild(p);
 
        }

        return card;
    },

    display : function (collection, location) {
        let container = document.querySelector(location);
        let card = this.buildCard(collection);
        container.appendChild(card);

        return true;
    }
}
