function loadData()
{
	fetch("https://jsonplaceholder.typicode.com/todos")
        .then( (resp) => resp.text() )
		.then( function (data) {
			let todoData= eval(data)
		    Todo.htmlList.display(todoData, '#root');
        })
}

loadData();