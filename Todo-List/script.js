// let todos = [
//     {
//         "title" : "Dinner With Mom",
//     },
//     {
//         "title" : "Hangout with friends",
//     },
//     {
//         "title" : "complete homeworks",
//     }
// ]
const Todo = {};

Todo.htmlList = {
    buildStatus : function () 
    {
        let input = document.createElement('input');
        input.setAttribute("type","checkbox");

        return input;
    },

    buildItem : function (title) 
    {
        let textNode = document.createTextNode(title);
        let input = this.buildStatus();
        let li = document.createElement('li');
        li.appendChild(input);
        li.appendChild(textNode);

        return li;
    },

    buildList : function (titles) 
    {
        let ul = document.createElement('ul');
        for (let title of titles) {
            let li = this.buildItem(title.title);
            ul.appendChild(li);
        }

        return ul;
    },

    display : function (titles, location) 
    {
        let container = document.querySelector(location);
        let ul = this.buildList(titles);
        container.appendChild(ul);

        return true;
    }
}
// Todo.orderList.display(todos, '#root');