 let calculate = {
    add : function(fisrtNumber,secondNumber){
        let add = Number(fisrtNumber) + Number(secondNumber);
        return add;
    },
    subs : function(fisrtNumber,secondNumber){
        let subs = fisrtNumber - secondNumber;
        return subs;
    },
    multi : function(fisrtNumber,secondNumber){
        let multi = fisrtNumber * secondNumber;
        return multi;
    },
    div : function(fisrtNumber,secondNumber){
        let div = fisrtNumber / secondNumber;
        return div;
    }
 }

let fisrtNum  =  document.getElementById('numOne');
let secondNum = document.getElementById('numTwo');

document.getElementById("sum").addEventListener('click', function(){
     let sumResult    = calculate.add(fisrtNum.value,secondNum.value);
     let sumResultShow =    document.getElementById('sumResult');
     sumResultShow.className = "d-block alert alert-success mt-5 ml-3";
     sumResultShow.innerHTML = "Summation Result is = " + sumResult; 
    });

document.getElementById("subs").addEventListener('click', function(){
    let subsResult     = calculate.subs(fisrtNum.value,secondNum.value);
    let subsResultShow =    document.getElementById('subsResult');
    subsResultShow.className = "d-block alert alert-info mt-5 ml-3";
    subsResultShow.innerHTML = "Subcription Result is = " + subsResult; 
});

document.getElementById("multi").addEventListener('click', function(){
    let multiResult     = calculate.multi(fisrtNum.value,secondNum.value);
    let multiResultShow =    document.getElementById('multiResult');
    multiResultShow.className = "d-block alert alert-warning mt-5 ml-3";
    multiResultShow.innerHTML = "Multipication Result is = " + multiResult; 
});

document.getElementById("div").addEventListener('click', function(){
    let divResult     = calculate.div(fisrtNum.value,secondNum.value);
    let divResultShow =    document.getElementById('divResult');
    divResultShow.className = "d-block alert alert-danger mt-5 ml-3";
    divResultShow.innerHTML = "Division Result is = " + divResult; 
});
